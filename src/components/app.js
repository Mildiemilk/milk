import React, {Component} from 'react';
import Header from './Header/Header';
import InputContainer from './InputContainer/InputContainer';
import TodoList from './TodoList/TodoList';



class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            task: '',
            age:''
        };

        this.onTaskAdd = this.onTaskAdd.bind(this);
    }

    onTaskAdd(value,detail){
        this.setState({task: value});
        this.setState({age: detail});
        
    }

    render() {
        return (
            <div>
                <Header />
                <InputContainer onTaskAdd={(value,detail)=>{this.onTaskAdd(value,detail)}}/>
                <TodoList task={this.state.task}
                          age={this.state.age}
                />

            </div>
        );
       
    }
    





}

export default App;