import React, {Component} from 'react';

class SearchBarTab extends Component {

    constructor(props) {
        super(props);

        this.state = {
            inputValue: ''
        }

    }

    handleOnClick() {
        console.log(this.state.inputValue)
    }

    onInputChange(e) {
        this.setState({inputValue: e.target.value});

    }

    render() {
        return (
            <div className="row">
                <input ref="todo-list-input" onChange={this.onInputChange.bind(this)} value={this.state.inputValue}
                       className="col-sm-8 form-control input-lg">
                    <button className="btn btn-default" onclick={this.handleOnClick.bind(this)}>Add</button>
                </input>

            </div>
        );

    }

}

export default SearchBarTab;
/**
 * Created by mildiemilk on 6/18/2016 AD.
 */
