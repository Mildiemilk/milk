import React,{Component} from 'react';

import moment from 'moment';

class TodoItem extends Component{
    constructor(props){
        super(props);
    }



    render() {

        var time = moment(this.props.date).format(this.props.format);

        return (

            <div>
                <div>{this.props.task}
                    {this.props.detail}
                    <span> {time} </span>

                </div>
                <button onClick={this.props.onClickClose}>delete</button>
            </div>
        );
    }
   


}

export default TodoItem;