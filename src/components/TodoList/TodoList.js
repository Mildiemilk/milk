import React,{Component} from 'react';

import TodoItem from './TodoItem';


class TodoList extends Component{
    constructor(props){
        super(props);

        this.state = {
            todoList:[],
            detail:[]
        };


        this.renderListItem = this.renderListItem.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let todoList = this.state.todoList;
        let detail = this.state.detail;


        todoList.push(nextProps.task);
        this.setState({todoList});

        detail.push(nextProps.age);
        this.setState({detail, detail});
    }

    removeItem(i) {
        this.state.todoList.splice(i, 1)
        this.state.detail.splice(i, 1)
        this.setState({
            todoList: this.state.todoList,
            detail: this.state.detail
        })
    }

    renderListItem(){
        return this.state.todoList.map((list,i)=>{

            return(
                <li key={i} >
                    <TodoItem
                        task={list}
                        detail={this.state.detail[i]}

                    onClickClose={this.removeItem.bind(this, i)}
                    />

                </li>
            )
        })

    }

    render(){
        return(
            <div>
                <ul>
                    {this.renderListItem()}

                </ul>
            </div>
        )

    }


}

export default TodoList;