import React,{Component} from 'react';



class InputContainer extends Component{

    constructor(props){
        super(props);

        this.onClickAddTask = this.onClickAddTask.bind(this);
    }

    onClickAddTask(){
        this.props.onTaskAdd(this.refs.addTaskInput1.value,this.refs.addTaskInput2.value);
        this.refs.addTaskInput1.value = "";
        this.refs.addTaskInput2.value = "";
    }

    

    render(){
        return(
            <div>
                <input className="form-control" ref="addTaskInput1" placeholder="Add name"/>
                <input ref="addTaskInput2" placeholder="Add age"/>


                <button className ="btn btn-success" onClick={this.onClickAddTask}>Add</button>
                
            </div>
        );
        
    }
   


}

export default InputContainer;